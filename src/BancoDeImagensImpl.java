import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

public class BancoDeImagensImpl implements BancoDeImagens {

	private Map<String, String> indice;
	
	public BancoDeImagensImpl() {
		indice = new HashMap<>();
		indice.put("fabio", "fabio.jpg");
		indice.put("obama", "obama.jpg");
		indice.put("palocci", "palocci.jpg");
	}
	
	public Image getImagem(String nome) {
		return carregarImagem(nome);
	}

	public List<String> getListaDeImagens() {
		return new ArrayList<>(indice.keySet());
	}

	private Image carregarImagem(String nome) {
		byte[] bytes = getRawBytes(nome);
		Image image = null;
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		Iterator<ImageReader> it = ImageIO.getImageReadersByFormatName("jpeg");
		ImageReader ir = (ImageReader) it.next();
		ImageInputStream iis;
		try {
			iis = ImageIO.createImageInputStream(bais);
			ir.setInput(iis, true);
			image = ir.read(0);
			bais.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return image;
	}
	
	private byte[] getRawBytes(String nome){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		File arquivo = new File(nome + ".jpg");
		try {
			FileInputStream fis = new FileInputStream(arquivo);
			int qtdeBytes;
			byte[] buffer = new byte[1024];
			while((qtdeBytes = fis.read(buffer)) != -1){
				baos.write(buffer, 0, qtdeBytes); 
			}
			fis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return baos.toByteArray();
	}
}