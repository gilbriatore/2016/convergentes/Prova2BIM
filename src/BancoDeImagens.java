import java.awt.Image;
import java.util.List;

public interface BancoDeImagens {
	Image getImagem(String nome);
	List<String> getListaDeImagens();
}